﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.Windows.Forms;

namespace Assignment_2_c3374267
{
    public partial class LoginForm : Form
    {
        public SqlCeConnection mySqlConnection;
        public LoginForm()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
        //settings
        }

        private bool Connect()
        {
            mySqlConnection =
        new SqlCeConnection(Globals.ConnectionStr);
            try
            {
                mySqlConnection.Open();
                return true;
            }

            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private bool Disconnect()
        {
            try
            {
                mySqlConnection.Close();
                return true;
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void Validation()
        {
        }

        public void cleartxtBoxes()
        {
            textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = "";
        }




        private void button3_Click(object sender, EventArgs e)
        {
            string user = textBox5.Text;
            string pass = textBox6.Text;

            if (this.Connect() == true)
            {
                String please = "SELECT * from Users WHERE Username = @Username AND Password = @Password";
                SqlCeCommand Please = new SqlCeCommand(please, mySqlConnection);
                Please.Parameters.AddWithValue("@Username", user);
                Please.Parameters.AddWithValue("@Password", pass);
                Please.ExecuteNonQuery();

                try
                {
         
                    SqlCeDataReader mySqlDataReader = Please.ExecuteResultSet(ResultSetOptions.Scrollable);
                    if (mySqlDataReader.HasRows)
                    {
                        MessageBox.Show("Login Success");
                        var Main = new Main();
                        Main.Show();
                        this.Hide();
                    }

                    else
                    {
                        MessageBox.Show("Login Failed");

                    }
                }
                catch (SqlCeException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        public void RegisterUser(String Username, String Password, String Name, String Age, String commandString)
        {
            SqlCeCommand Register = new SqlCeCommand(commandString, mySqlConnection);

            try
            {
                Register.Parameters.AddWithValue("@Username", Username);
                Register.Parameters.AddWithValue("@Password", Password);
                Register.Parameters.AddWithValue("@Name", Name);
                Register.Parameters.AddWithValue("@Age", Age);
                Register.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (this.Connect() == true)
            {
                String commandString = "INSERT INTO Users(Username, Password, Name, Age) VALUES (@Username, @Password, @Name, @Age)";
                RegisterUser(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, commandString);
                cleartxtBoxes();
                MessageBox.Show("Insert Successful");
                this.DialogResult = DialogResult.OK;

                this.Close();

            }

            else
            {
                MessageBox.Show("Nein");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


     



        //button


    }
}
