﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;

namespace Assignment_2_c3374267
{
    public static class Globals
    {
        public const string ConnectionStr = @"Data Source=H:\Year 3 Semester 1\Software\Assignment2.sdf"; // Public const string
        public const int Width = 700; // Public const int
        public const int Height = 600; // Public const int
        public const int NameLength = 20; // Public const int
        public const int DescriptionLength = 50; // Public const int
        public const int PasswordLength = 15; // Public const int
        public const int UsernameLength = 15; // Public const int
        public const int AgeLength = 3; // Public const int
        public const int EmailLength = 300; // Public const int
        public const int LabelHeadingFontSize = 20; // Public const int
    }
}
