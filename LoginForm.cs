﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Assignment_2_c3374267
{
    public partial class LoginForm : Form
    {
        DB DB = new DB(); // Instantiates the DB class
        public static string sendUsername; // Public static string sendUsername
        public static string sendID; // Public static string sendID
        Regex myRegularExpression = new Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"); // New regular expression for email

        public LoginForm()
        {
            InitializeComponent();
            init(); // Initializes init();
        }

        public void init() // init method
        {
            toolStripStatusLabel1.Text = "Alert: Please login or register!"; // Output alert message
            this.Text = "Login Form"; // Sets Form title to this
            this.Width = Globals.Width; // Sets Form Width to Globals.Width
            this.Height = Globals.Height; // Sets Form Height to Globals.Height
            RegisterUsername.MaxLength = Globals.UsernameLength; // Uses Globals.UsernameLength to set max length of textbox
            LoginUsername.MaxLength = Globals.UsernameLength; // Uses Globals.UsernameLength to set max length of textbox
            LoginPassword.MaxLength = Globals.PasswordLength; // Uses Globals.PasswordLength to set max length of textbox
            RegisterPassword.MaxLength = Globals.PasswordLength; // Uses Globals.PasswordLength to set max length of textbox
            RegisterFirstname.MaxLength = Globals.NameLength; // Uses Globals.NameLength to set max length of textbox
            RegisterLastname.MaxLength = Globals.NameLength; // Uses Globals.NameLength to set max length of textbox
            RegisterAge.MaxLength = Globals.AgeLength; // Uses Globals.AgeLength to set max length of textbox
            RegisterEmail.MaxLength = Globals.EmailLength; // Uses Globals.EmailLength to set max length of textbox 
            RegisterLabel.Font = new Font(RegisterLabel.Font.FontFamily, Globals.LabelHeadingFontSize); // Uses Globals.LabelHeadingFontSize to set label font size
            LoginLabel.Font = new Font(LoginLabel.Font.FontFamily, Globals.LabelHeadingFontSize); // Uses Globals.LabelHeadingFontSize to set label font size
        }

        public void CleartxtBoxes() // CleartxtBoxes method
        {
            LoginUsername.Text = LoginPassword.Text = ""; // Clears textbox values
            RegisterUsername.Text = RegisterPassword.Text = RegisterFirstname.Text = RegisterLastname.Text = RegisterAge.Text = RegisterEmail.Text = ""; // Clears textbox values
        }

        public bool ValidateLogin() // ValidateLogin method
        {
            bool rtnvalue = true; // Sets bool rtnvalue to true

            // Checks if textbox values are null or empty
            if (string.IsNullOrEmpty(LoginUsername.Text) ||
                string.IsNullOrEmpty(LoginPassword.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all Login fields"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            return (rtnvalue); // Returns true
        }

        public bool ValidateRegister() // ValidateRegister method
        {
            bool rtnvalue = true; // Sets bool rtnvalue to true

            // Checks if textbox values are null or empty
            if (string.IsNullOrEmpty(RegisterUsername.Text) ||
                string.IsNullOrEmpty(RegisterPassword.Text) ||
                string.IsNullOrEmpty(RegisterFirstname.Text) ||
                string.IsNullOrEmpty(RegisterLastname.Text) ||
                string.IsNullOrEmpty(RegisterAge.Text) ||
                string.IsNullOrEmpty(RegisterEmail.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all Register fields"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (RegisterUsername.Text.Length < 4) // Checks if RegisterUsername is less than 4 characters
            {
                toolStripStatusLabel1.Text = "Error: Username must contain more than four characters"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (RegisterUsername.Text.Length >= 16) // Checks if RegisterUsername is more than 15 characters
            {
                toolStripStatusLabel1.Text = "Error: Username must contain less than fifteen characters"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (RegisterPassword.Text.Length < 6) // Checks if RegisterPassword is less than 6 characters
            {
                toolStripStatusLabel1.Text = "Error: Password must contain more than six characters"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (RegisterPassword.Text.Length >= 16) // Checks if RegisterPassword is more than 15 characters
            {
                toolStripStatusLabel1.Text = "Error: Password must contain less than 15 characters"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (!RegisterAge.Text.All(c => Char.IsNumber(c))) // Checks if RegisterAge is a number
            {
                toolStripStatusLabel1.Text = "Error: Age must be a number"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            else if (!myRegularExpression.IsMatch(RegisterEmail.Text)) // Checks if RegisterEmail is a valid email address
            {
                toolStripStatusLabel1.Text = "Error: Invalid Email"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            return (rtnvalue); // Returns true
        }

        private void Login_Click(object sender, EventArgs e) // Login button click
        {
            sendUsername = LoginUsername.Text; // Stores LoginUser text in sendUsername (public string)
            string pass = LoginPassword.Text; // Stores LoginPassword text in string pass

            if (ValidateLogin()) // Checks if login textbox input values are valid
            {
                if (DB.Connect() == true) // Checks for database connection using the DB class
                {
                    String select = "SELECT * FROM Users WHERE Username = @Username AND Password = @Password"; // SQL Query: Selects all records in Users
                    SqlCeCommand LoginCheck = new SqlCeCommand(select, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand LoginCheck with select and DB.mySqlConnection
                    LoginCheck.Parameters.AddWithValue("@Username", sendUsername); // Sets username value from texbox
                    LoginCheck.Parameters.AddWithValue("@Password", pass); // Sets password value from textbox
                    LoginCheck.ExecuteNonQuery(); // Executes SQL query

                    try
                    {
                        SqlCeDataReader mySqlDataReader = LoginCheck.ExecuteResultSet(ResultSetOptions.Scrollable); //

                        if (mySqlDataReader.HasRows) // Checks if users table has rows
                        {
                            while (mySqlDataReader.Read()) // Creates instance of SqlCeDataReader
                            {
                                HiddenUserID.Text = mySqlDataReader["UserID"].ToString(); // Gets UserID from users table and stores it in the HiddenUserID textbox
                            }

                            sendID = HiddenUserID.Text; // Stores HiddenUserID value in sendID (public static string)
                            CleartxtBoxes(); // Clears textboxs
                            var Main = new MainForm(); /// Instantiates MainForm
                            Main.Show(); // Shows MainForm
                            this.Hide(); // Hides this form
                        }

                        else
                        {
                            toolStripStatusLabel1.Text = "Error: Login Failed"; // Outputs error message
                        }
                    }
                    catch (SqlCeException ex) // Catches exceptions
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                else
                {
                    DB.Disconnect(); // Calls DB.Disconnect() method
                }
            }
        }

        public void RegisterUser(String Username, String Password, String Firstname, String Lastname, String Age, String Email, String commandString) // RegisterUser method
        {
            SqlCeCommand Register = new SqlCeCommand(commandString, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand Register with commandString and DB.mySqlConnection

            try
            {
                Register.Parameters.AddWithValue("@Username", Username); // Inserts Username
                Register.Parameters.AddWithValue("@Password", Password); // Inserts Password
                Register.Parameters.AddWithValue("@Firstname", Firstname); // Inserts Firstname
                Register.Parameters.AddWithValue("@Lastname", Lastname); // Inserts Lastname
                Register.Parameters.AddWithValue("@Age", Age); // Inserts Age
                Register.Parameters.AddWithValue("@Email", Email); // Inserts Email
                Register.ExecuteNonQuery(); // Executes SQL query
            }
            catch (SqlCeException ex) // Catches exceptions
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Register_Click(object sender, EventArgs e) // Register button click
        {
            if (DB.Connect() == true) // Checks for database connection using the DB class
            {
                if (ValidateRegister()) // Checks if register textbox input values are valid
                {
                    String commandString = "INSERT INTO Users(Username, Password, Firstname, Lastname, Age, Email) VALUES (@Username, @Password, @Firstname, @Lastname, @Age, @Email)"; // SQL Query: Inserts record into the Users table
                    RegisterUser(RegisterUsername.Text, RegisterPassword.Text, RegisterFirstname.Text, RegisterLastname.Text, RegisterAge.Text, RegisterEmail.Text, commandString); // Uses the RegisterUser method to execute the query
                    CleartxtBoxes(); // Clears textboxs
                    toolStripStatusLabel1.Text = "Success: Sign up complete, you may now log in"; // Outputs error message
                }
            }

            else
            {
                DB.Disconnect(); // Calls DB.Disconnect() method
            }
        }

        private void Exit_Click(object sender, EventArgs e) // Exit button click
        {
            Application.Exit(); // Exits application
        }
    }
}
