﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlServerCe;
using System.Windows.Forms;

namespace Assignment_2_c3374267
{
    public class DB
    {
        public String total;
        public SqlCeConnection mySqlConnection; // Represents connection to the database

        public bool Connect() // Connect to the database
        {
            mySqlConnection = new SqlCeConnection(Globals.ConnectionStr); // Uses Globals.ConnectionStr to set mySqlConnection
            try
            {
                mySqlConnection.Open(); // Opens database connection
                return true;
            }

            catch (SqlCeException ex) // Catches exceptions
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool Disconnect() // Disconnect from the database
        {
            try
            {
                mySqlConnection.Close(); // Closes database connection
                return true;
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message); // Catches exceptions
                return false;
            }
        }
    }
}

