﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;


namespace Assignment_2_c3374267
{
    public partial class Main : Form
    {
        public SqlCeConnection mySqlConnection;
        private string now = null;

        public Main()
        {
            InitializeComponent();
            init();
            Connect();
            populateListView(); 
        }

        public void init()
        {
            this.Width = 700;
            this.Height = 600;
            textBoxName.MaxLength = 20;
            textBoxDescription.MaxLength = 30;
            panel2.Visible = false;
            panel3.Visible = false;
            string[] resolved = new string[]{"Yes", "No"};
            comboBox1.Items.AddRange(resolved);

            listView1.Columns.Add("BugID", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("BugName", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("BugDescription", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("BugDate", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("Resolved", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("Blank", 150, HorizontalAlignment.Center);
            listView1.Columns.RemoveAt(5);
        }

        private bool Connect()
        {
            mySqlConnection =
        new SqlCeConnection(Globals.ConnectionStr);
            try
            {
                mySqlConnection.Open();
                return true;
            }

            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private bool Disconnect()
        {
            try
            {
                mySqlConnection.Close();
                return true;
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void populateListView()
        {
            if (this.Connect() == true)
            {
                String selcmd = "SELECT * from Bugs";

                SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

                try
                {

                    SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                    listView1.Items.Clear();

                    listView1.View = View.Details;

                    listView1.LabelEdit = true;

                    // Allow the user to rearrange columns.
                    listView1.AllowColumnReorder = true;

                    // Select the item and subitems when selection is made.
                    listView1.FullRowSelect = true;

                    // Display grid lines.
                    listView1.GridLines = true;

                    // Sort the items in the list in ascending order.
                    listView1.Sorting = SortOrder.Ascending;

                    while (mySqlDataReader.Read())
                    {
                        var item = new ListViewItem();
                        item.Text = mySqlDataReader["BugID"].ToString();        // 1st column text
                        item.SubItems.Add(mySqlDataReader["BugName"].ToString());  // 2nd column text
                        item.SubItems.Add(mySqlDataReader["BugDescription"].ToString());
                        item.SubItems.Add(mySqlDataReader["BugDate"].ToString());
                        item.SubItems.Add(mySqlDataReader["Resolved"].ToString());
                        listView1.Items.Add(item);
                    }
                }

                catch (SqlCeException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        

        public void cleartxtBoxes()
        {
            textBoxName.Text = textBoxDescription.Text = "";
        }

        public bool ValidateInsert()
        {
            bool rtnvalue = true;
     
            if (string.IsNullOrEmpty(textBoxName.Text) ||
                string.IsNullOrEmpty(textBoxDescription.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all fields";
                rtnvalue = false;
            }

            return (rtnvalue);
        }

        public bool ValidateUpdate()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(textBox1.Text) ||
                string.IsNullOrEmpty(UpdateDescription.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all fields";
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void Insert(String Name, String Description, String Date, String commandString)
        {
                DateTime now = DateTime.Now;
                SqlCeCommand Insert = new SqlCeCommand(commandString, mySqlConnection);

                try
                {
                    Insert.Parameters.AddWithValue("@Name", Name);
                    Insert.Parameters.AddWithValue("@Description", Description);
                    Insert.Parameters.AddWithValue("@Date", now);
                    Insert.ExecuteNonQuery();
                }
                catch (SqlCeException ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void InsertBug_Click(object sender, EventArgs e)
        {
            if (ValidateInsert())
            {
                if (this.Connect() == true)
                {
                    String commandString = "INSERT INTO Bugs(BugName, BugDescription, BugDate) VALUES (@Name, @Description, @Date)";
                    Insert(textBoxName.Text, textBoxDescription.Text, now, commandString);
                    cleartxtBoxes();
                    panel2.Visible = false;
                    panel1.Visible = true;
                    populateListView();
                }
            }
        }

        private void DeleteSelectedBug_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
            {
                toolStripStatusLabel1.Text = "Error: Select an Item to delete";
            }
            else
            {
                if (this.Connect() == true)
                {
                    foreach (ListViewItem item in listView1.SelectedItems)
                    {
                        String test = "DELETE FROM Bugs WHERE BugID = @BugID";
                        SqlCeCommand Test = new SqlCeCommand(test, mySqlConnection);
                        Test.Parameters.AddWithValue("@BugID", item.Text);
                        Test.ExecuteNonQuery();
                        populateListView();
                        toolStripStatusLabel1.Text = "Success: Delete Successful";
                    }
                }
                else
                {
                    this.Disconnect();
                }
            }
        }

        private void UpdateBug_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
            {
                toolStripStatusLabel1.Text = "Error: Select an Item to Edit";
            }
            else if (listView1.SelectedItems.Count >= 2)
            {
                toolStripStatusLabel1.Text = "Error: Select only one Item";
            }   
            else
            {
                panel2.Visible = true;
                panel3.Visible = true;
                foreach (ListViewItem item in listView1.SelectedItems)
                {
                    textBox3.Text = item.SubItems[0].Text;
                    textBox1.Text = item.SubItems[1].Text;
                    UpdateDescription.Text = item.SubItems[2].Text;
                    comboBox1.Text = item.SubItems[4].Text;

                }
            }
        }

        private void UpdateSelected_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;

            if (ValidateUpdate()){
                try
                {
                    String commandString = "UPDATE Bugs SET BugName = @Name, BugDescription = @Description, BugDate = @Date, Resolved = @Resolved WHERE BugID = @ID";
                    SqlCeCommand Update = new SqlCeCommand(commandString, mySqlConnection);
                    Update.Parameters.AddWithValue("@ID", textBox3.Text);
                    Update.Parameters.AddWithValue("@Name", textBox1.Text);
                    Update.Parameters.AddWithValue("@Description", UpdateDescription.Text);
                    Update.Parameters.AddWithValue("@Date", now);
                    Update.Parameters.AddWithValue("@Resolved", comboBox1.Text);
                    Update.ExecuteNonQuery();
                    cleartxtBoxes();
                    populateListView();
                    panel2.Visible = false;
                    panel1.Visible = true;
                    String ID = textBox3.Text;
                    toolStripStatusLabel1.Text = "Success: BugID: " + ID + " Was updated";
                }
                catch (SqlCeException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void deleteAllResolvedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count == 0)
            {
                toolStripStatusLabel1.Text = "Error: No Bugs Resolved";
            }
            else
            {
                String check = "SELECT COUNT(*) FROM Bugs WHERE Resolved = 'Yes'";
                String delete = "DELETE FROM Bugs WHERE Resolved = 'Yes'";

                    if (listView1.Items.Count >= 1)
                    {
                        SqlCeCommand Check = new SqlCeCommand(check, mySqlConnection);
                        Check.ExecuteNonQuery();
                        int rows = Convert.ToInt32(Check.ExecuteScalar());
                        
                        if (rows < 1)
                        {
                            toolStripStatusLabel1.Text = "Error: Nothing to Delete";
                        }
                        else
                        {
                            SqlCeCommand Delete = new SqlCeCommand(delete, mySqlConnection);
                            Delete.ExecuteNonQuery();
                            populateListView();
                            toolStripStatusLabel1.Text = "Success: Resolved Delete Successful";
                        }
                        
                    }
            }
        }

        private void homeToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel1.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void insertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            panel3.Visible = false;
        }

    }
    }



