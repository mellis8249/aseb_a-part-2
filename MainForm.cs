﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Text.RegularExpressions;


namespace Assignment_2_c3374267
{
    public partial class MainForm : Form
    {
        private string dateTime = null; // Private string for dateTime
        DB DB = new DB(); // Instantiates the DB class
        public int total; // Public int total
        public int unresolved; // Public int unresolved
        
        public MainForm()
        {
            InitializeComponent();
            init(); // Initializes init();
            Welcome(); // Initializes Welcome();
            populateListView(); // Initializes populateListView();  
        }

        public void init() // init method
        {
            this.Text = "Main Form"; // Sets Form title to this
            this.Width = Globals.Width; // Sets Form Width to Globals.Width
            this.Height = Globals.Height; // Sets Form Height to Globals.Height
            textBoxInsertBugName.MaxLength = Globals.NameLength; // Uses Globals.NameLength to set max length of textbox
            textBoxUpdateBugName.MaxLength = Globals.NameLength; // Uses Globals.NameLength to set max length of textbox
            textBoxInsertBugDescription.MaxLength = Globals.DescriptionLength; // Uses Globals.DescriptionLength to set max length of textbox
            textBoxUpdateBugDescription.MaxLength = Globals.DescriptionLength; // Uses Globals.DescriptionLength to set max length of textbox
            InsertLabel.Font = new Font(InsertLabel.Font.FontFamily, Globals.LabelHeadingFontSize); // Uses Globals.LabelHeadingFontSize to set label font size
            UpdateLabel.Font = new Font(UpdateLabel.Font.FontFamily, Globals.LabelHeadingFontSize); // Uses Globals.LabelHeadingFontSize to set label font size
            
            // Sets InsertPanel and UpdatePanel visibility
            InsertPanel.Visible = false;
            UpdatePanel.Visible = false;

            string[] resolved = new string[]{"Yes", "No"}; // Creates a string array
            comboBoxUpdateBugResolved.Items.AddRange(resolved); // Sets the comboBox's items to the string array values

            string[] priority = new string[] { "Low", "Medium", "High" }; // Creates a string array
            comboBoxInsertPriority.Items.AddRange(priority); // Sets the comboBox's items to the string array values
            comboBoxUpdatePriority.Items.AddRange(priority); // Sets the comboBox's items to the string array values

            // Adds and sets columns to listView1
            listView1.Columns.Add("BugID", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("BugName", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("BugDescription", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("BugDate", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("Priority", 75, HorizontalAlignment.Center);
            listView1.Columns.Add("Resolved", 75, HorizontalAlignment.Center);
            listView1.Columns.Add("Blank", 150, HorizontalAlignment.Center);
            listView1.Columns.RemoveAt(6); //Removes blank column
        }

        public void Welcome() // Welcome method
        {
            string username = LoginForm.sendUsername; // Gets username from LoginForm and stores it as a string
            toolStripStatusLabel1.Text = "Success: Welcome " + username; // Displays welcome message and username of user who logged in
        }

        public void CleartxtBoxes() // CleartxtBoxes method
        {
            textBoxInsertBugName.Text = textBoxInsertBugDescription.Text = ""; // Clears textbox values
        }

        public bool ValidateInsert() // ValidateInsert method
        {
            bool rtnvalue = true; // Sets bool rtnvalue to true

            // Checks if textbox values are null or empty
            if (string.IsNullOrEmpty(textBoxInsertBugName.Text) ||
                string.IsNullOrEmpty(textBoxInsertBugDescription.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all fields"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            return (rtnvalue); // Returns true
        }

        public bool ValidateUpdate() // Validate update textbox's
        {
            bool rtnvalue = true; // Sets bool rtnvalue to true

            // Checks if textbox values are null or empty
            if (string.IsNullOrEmpty(textBoxUpdateBugName.Text) ||
                string.IsNullOrEmpty(textBoxUpdateBugDescription.Text))
            {
                toolStripStatusLabel1.Text = "Error: Fill in all fields"; // Outputs error message
                rtnvalue = false; // Sets return value to false
            }

            return (rtnvalue); // Returns true
        }

        public void populateListView() // Displays Bugs to listView1
        {
            if (DB.Connect() == true) // Checks for database connection using the DB class
            {
               string ID = LoginForm.sendID; // Gets UserID from LoginForm and stores it as a string
               String select = "SELECT * from Bugs WHERE UserID = " + ID; // SQL Query: Selects all bugs from the database where the UserID matches the current user logged in.
               SqlCeCommand mySqlCommand = new SqlCeCommand(select, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand with SQL query and DB.mySqlConnection

                try
                {
                    SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader(); // Creates instance of SqlCeDataReader

                    listView1.Items.Clear(); // Clears listView1 items.
                    listView1.View = View.Details; // Specify's how listView1 is displayed
                    listView1.LabelEdit = false; // listView1 can't be edit'd
                    listView1.AllowColumnReorder = true;  // Allow the user to rearrange columns.
                    listView1.FullRowSelect = true;  // Select the item and subitems when selection is made.
                    listView1.GridLines = true; // Displays grid lines.
                    listView1.Sorting = SortOrder.Ascending; // Sorts the items in listView1 to an ascending order.

                    while (mySqlDataReader.Read()) // Gets records from the database
                    {
                        var item = new ListViewItem(); //Creates new ListViewItem
                        item.Text = mySqlDataReader["BugID"].ToString(); // 1st column text
                        item.SubItems.Add(mySqlDataReader["BugName"].ToString());  // 2nd column text
                        item.SubItems.Add(mySqlDataReader["BugDescription"].ToString()); // 3rd column text
                        item.SubItems.Add(mySqlDataReader["BugDate"].ToString()); // 4th column text
                        item.SubItems.Add(mySqlDataReader["Priority"].ToString()); // 5th column text
                        item.SubItems.Add(mySqlDataReader["Resolved"].ToString()); // 6th column text
                        listView1.Items.Add(item);  // Adds items to listView1
                    }
                }
                catch (SqlCeException ex) // Catches exceptions
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
               DB.Disconnect(); // Calls DB.Disconnect() method
            }
        }

        public void Insert(String Name, String Description, String Date, String Priority, int UserID, String commandString) // 
        {
            DateTime dateTime = DateTime.Now; // Gets local machine date and time
            SqlCeCommand Insert = new SqlCeCommand(commandString, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand Insert with commandString and DB.mySqlConnection
            
            try
            {
                Insert.Parameters.AddWithValue("@Name", Name); // Inserts BugName
                Insert.Parameters.AddWithValue("@Description", Description); // Inserts BugDescription
                Insert.Parameters.AddWithValue("@Date", dateTime); // Inserts dateTime
                Insert.Parameters.AddWithValue("@Priority", Priority); // Inserts priority
                Insert.Parameters.AddWithValue("@UserID", UserID); // Inserts UserID
                Insert.ExecuteNonQuery(); // Executes SQL query
            }
            catch (SqlCeException ex) // Catches exceptions
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InsertBug_Click(object sender, EventArgs e) // InsertBug button click
        {
            if (ValidateInsert()) // Checks if update textbox input values are valid
            {
                if (DB.Connect() == true) // Checks for database connection using the DB class
                {
                    String getID = LoginForm.sendID; // Gets UserID from LoginForm and stores it as a string
                    int ID = Convert.ToInt32(getID); // Converts UserID to integer
                    String commandString = "INSERT INTO Bugs(BugName, BugDescription, BugDate, Priority, UserID) VALUES (@Name, @Description, @Date, @Priority, @UserID)"; // SQL Query: Inserts record into the Bugs table
                    Insert(textBoxInsertBugName.Text, textBoxInsertBugDescription.Text, dateTime, comboBoxInsertPriority.Text, ID, commandString); // Uses the Insert method to execute the query
                    CleartxtBoxes(); // Clears textboxs
                    InsertPanel.Visible = false; // Sets InsertPanel visibility
                    MainPanel.Visible = true; // Sets MainPanel visibility
                    populateListView(); // Calls populateListView() method to update listView1
                    toolStripStatusLabel1.Text = "Success: Insert Successful"; // Success message
                }
                else
                {
                    DB.Disconnect(); // Calls DB.Disconnect() method
                }
            }
        }

        private void UpdateBug_Click(object sender, EventArgs e) // UpdateBug button click
        {
            if (listView1.SelectedItems.Count <= 0) // Checks if there are no items in listView1
            {
                toolStripStatusLabel1.Text = "Error: Select an Item to Edit"; // Outputs error message
            }
            else if (listView1.SelectedItems.Count >= 2) // Checks if user selected more than one item in listView1
            {
                toolStripStatusLabel1.Text = "Error: Select only one Item"; // Outputs error message
            } 
            else 
            {
                toolStripStatusLabel1.Text = "Update: Use the form to Update selected Bug";
                InsertPanel.Visible = true; // Sets InsertPanel visibility
                UpdatePanel.Visible = true; // Sets UpdatePanel visibility
                foreach (ListViewItem item in listView1.SelectedItems) // Gets item selected in listView1
                {
                    textBoxHiddenBugID.Text = item.SubItems[0].Text; // Sets textBox to first item in listView1
                    textBoxUpdateBugName.Text = item.SubItems[1].Text; // Sets textBox to second item in listView1
                    textBoxUpdateBugDescription.Text = item.SubItems[2].Text; // Sets textBox to third item in listView1
                    comboBoxUpdatePriority.Text = item.SubItems[4].Text; // Sets comboBox to fifth item in listView1
                    comboBoxUpdateBugResolved.Text = item.SubItems[5].Text; // Sets comboBox to sixth item in listView1
                }
            }
        }

        private void UpdateSelected_Click(object sender, EventArgs e) // UpdateSelected button click
        {
            DateTime DateTime = DateTime.Now; // Gets local machine date and time

            if (DB.Connect() == true) // Checks for database connection using the DB class
            {
                if (ValidateUpdate()) // Checks if update textbox input values are valid
                {
                    try
                    {
                        String commandString = "UPDATE Bugs SET BugName = @Name, BugDescription = @Description, BugDate = @Date, Priority = @Priority, Resolved = @Resolved WHERE BugID = @ID"; // SQL Query: Update Bugs
                        SqlCeCommand Update = new SqlCeCommand(commandString, DB.mySqlConnection); // SQL Command: 
                        Update.Parameters.AddWithValue("@ID", textBoxHiddenBugID.Text); // Updates BugID
                        Update.Parameters.AddWithValue("@Name", textBoxUpdateBugName.Text); // Updates BugName
                        Update.Parameters.AddWithValue("@Description", textBoxUpdateBugDescription.Text); // Updates BugDescription
                        Update.Parameters.AddWithValue("@Date", DateTime); // Updates DateTime
                        Update.Parameters.AddWithValue("@Priority", comboBoxUpdatePriority.Text); // Updates Priority
                        Update.Parameters.AddWithValue("@Resolved", comboBoxUpdateBugResolved.Text); // Updates Resolved
                        Update.ExecuteNonQuery(); // Executes SQL query
                        CleartxtBoxes(); // Clears textboxs
                        populateListView(); // Calls populateListView() method to update listView1
                        InsertPanel.Visible = false; // Sets InsertPanel visibility
                        MainPanel.Visible = true; // Sets MainPanel visibility
                        String ID = textBoxHiddenBugID.Text; // Sets string ID to textBoxHiddenBugID text
                        toolStripStatusLabel1.Text = "Success: BugID: " + ID + " Was updated"; // Outputs success message
                    }
                    catch (SqlCeException ex) // Catches exceptions
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else
            {
                DB.Disconnect(); // Calls DB.Disconnect() method
            }
        }

        private void DeleteSelectedBug_Click(object sender, EventArgs e) // Delete selected bug button click
        {
            if (listView1.SelectedItems.Count <= 0) // Checks if there are no items in listView1
            {
                toolStripStatusLabel1.Text = "Error: Select an Item to delete"; // Outputs error message
            }
            else
            {
                if (DB.Connect() == true) // Checks for database connection using the DB class
                {
                    foreach (ListViewItem item in listView1.SelectedItems) // Gets item selected in listView1
                    {
                        String SelectDelete = "DELETE FROM Bugs WHERE BugID = @BugID"; // SQL Query: Deletes from Bugs
                        SqlCeCommand selectDelete = new SqlCeCommand(SelectDelete, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand selectDelete with SelectDelete and DB.mySqlConnection
                        selectDelete.Parameters.AddWithValue("@BugID", item.Text); // Deletes record matching BugID in listView1 
                        selectDelete.ExecuteNonQuery(); // Executes SQL query
                        populateListView(); // Calls populateListView() method to update listView1
                        toolStripStatusLabel1.Text = "Success: Delete Successful"; // Outputs success message
                    }
                }
                else
                {
                    DB.Disconnect(); // Calls DB.Disconnect() method
                }
            }
        }

        private void deleteAllResolvedToolStripMenuItem_Click(object sender, EventArgs e) // DeleteAllResolved menu click
        {
            if (DB.Connect() == true) // Checks for database connection using the DB class
            {
                if (listView1.Items.Count == 0) // Checks if there are any items in listView1
                {
                    toolStripStatusLabel1.Text = "Error: No Bugs Resolved"; // Outputs error message
                }
                else
                {
                    string getID = LoginForm.sendID; // Gets UserID from LoginForm stores as a string
                    int ID = Convert.ToInt32(getID); // Converts UserID to integer
                    String check = "SELECT COUNT(*) FROM Bugs WHERE Resolved = 'Yes' AND UserID = " + ID; // SQL Query: Counts records in bugs
                    String delete = "DELETE FROM Bugs WHERE Resolved = 'Yes' AND UserID = " + ID; // SQL Query: Deletes from bugs
                    if (listView1.Items.Count >= 1) // Checks if listView1 contains items
                    {
                        SqlCeCommand Check = new SqlCeCommand(check, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand Check with check and DB.mySqlConnection
                        Check.ExecuteNonQuery(); // Executes SQL query
                        int rows = Convert.ToInt32(Check.ExecuteScalar()); // Returns first column of bug table and converts to integer stores in rows

                        if (rows < 1) // Cheecks if rows are less than 1
                        {
                            toolStripStatusLabel1.Text = "Error: Nothing to Delete"; // Outputs error message
                        }
                        else
                        {
                            SqlCeCommand Delete = new SqlCeCommand(delete, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand Delete with delete and DB.mySqlConnection
                            Delete.ExecuteNonQuery(); // Executes SQL query
                            populateListView(); // Calls populateListView() method to update listView1
                            toolStripStatusLabel1.Text = "Success: All Resolved deleted"; // Outputs success message
                        }
                    }
                }
            }
            else
            {
                DB.Disconnect(); // Calls DB.Disconnect() method
            }
        }

        public void Statistics()
        {
            string ID = LoginForm.sendID; //
            String getAll = "SELECT COUNT(*) FROM Bugs WHERE UserID = " + ID; // SQL Query: Counts records in bugs
            SqlCeCommand totalRows = new SqlCeCommand(getAll, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand totalRows with getAll and DB.mySqlConnection
            totalRows.ExecuteNonQuery(); // Executes SQL query
            total = Convert.ToInt32(totalRows.ExecuteScalar()); // Gets total number of rows and stores in total

            String getAllUnresolved = "SELECT COUNT(*) FROM Bugs WHERE Resolved = 'No' AND UserID = " + ID; // SQL Query: Counts records that are unresolved in bugs
            SqlCeCommand totalUnresolved = new SqlCeCommand(getAllUnresolved, DB.mySqlConnection); // SQL Command: Creates SqlCeCommand totalUnresolved with Unresolved and DB.mySqlConnection
            totalUnresolved.ExecuteNonQuery(); // Executes SQL query
            unresolved = Convert.ToInt32(totalUnresolved.ExecuteScalar()); // Gets total number of unresolved rows and stores in total
        }

        private void homeToolStripMenuItem_Click_2(object sender, EventArgs e) // Home menu click
        {
            InsertPanel.Visible = false; // Sets InsertPanel visibility
            MainPanel.Visible = true; // Sets UpdatePanel visibility
            string username = LoginForm.sendUsername; // Gets username from LoginForm and stores it as a string
            toolStripStatusLabel1.Text = "Alert: Welcome Home " + username; // Output alert message
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e) // Logout menu click
        {
            var Login = new LoginForm(); // Instantiates LoginForm class
            this.Hide(); // Hides this form
            Login.Show(); // Shows LoginForm
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e) // Exit menu click
        {
            Application.Exit(); // Exits application
        }

        private void insertToolStripMenuItem_Click(object sender, EventArgs e) // Insert menu click
        {
            InsertPanel.Visible = true; // Sets InsertPanel visibility
            UpdatePanel.Visible = false; // Sets UpdatePanel visibility
            string username = LoginForm.sendUsername; // Gets username from LoginForm and stores it as a string
            toolStripStatusLabel1.Text = "Insert: Use the form to Insert a Bug"; // Output insert message
        }

        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Statistics(); // Call Statistics method
            MessageBox.Show("Statistics" + Environment.NewLine + "Total Bugs: " + total + Environment.NewLine + "Unresolved: " + unresolved); // Outputs statistics

        }

        protected override void OnFormClosing(FormClosingEventArgs e) // Overides OnFormClosing
        {
            Application.Exit(); // Exits application
        }
    }
}



